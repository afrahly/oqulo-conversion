import React from 'react';
import '../scss/Section4.scss';

function Section4() {
    return (
        <div className='section4'>
            <div className='section4-content'>
                <h4>Launching Soon</h4>
                <h3>Sign up to get updates on Oqulo’s public release.</h3>
                <form action="">
                    
                </form>
            </div>
        </div>
    )
}

export default Section4;
